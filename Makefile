#
# Project makefile for a Tango project. You should normally only need to modify
# PROJECT below.
#

#
# CAR_OCI_REGISTRY_HOST and PROJECT are combined to define
# the Docker tag for this project. The definition below inherits the standard
# value for CAR_OCI_REGISTRY_HOST = artefact.skao.int and overwrites
# PROJECT to give a final Docker tag of
# artefact.skao.int/ska-tango-examples/powersupply
#
PROJECT = ska-mid-cbf-engineering-console

# KUBE_NAMESPACE defines the Kubernetes Namespace that will be deployed to
# using Helm.  If this does not already exist it will be created
KUBE_NAMESPACE ?= ska-mid-cbf
DOMAIN ?= cluster.local

# RELEASE_NAME is the release that all Kubernetes resources will be labelled
# with
RELEASE_NAME ?= test

OCI_IMAGES ?= ska-mid-cbf-engineering-console ska-mid-cbf-engineering-console-rdma
OCI_IMAGES_TO_PUBLISH ?= $(OCI_IMAGES)
OCI_IMAGE_BUILD_CONTEXT = $(PWD)

# use python3 if not CI job
ifeq ($(strip $(CI_JOB_ID)),)
PYTHON_RUNNER = python3 -m
endif


# Fixed variables
# Timeout for gitlab-runner when run locally
TIMEOUT = 86400

CI_PROJECT_DIR ?= .

XAUTHORITY ?= $(HOME)/.Xauthority
THIS_HOST := $(shell ip a 2> /dev/null | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)
DISPLAY ?= $(THIS_HOST):0
JIVE ?= true# Enable jive
WEBJIVE ?= false# Enable Webjive

CI_PROJECT_PATH_SLUG ?= ska-mid-cbf-engineering-console
CI_ENVIRONMENT_SLUG ?= ska-mid-cbf-engineering-console
$(shell echo 'global:\n  annotations:\n    app.gitlab.com/app: $(CI_PROJECT_PATH_SLUG)\n    app.gitlab.com/env: $(CI_ENVIRONMENT_SLUG)' > gitlab_values.yaml)

# define private overrides for above variables in here
-include PrivateRules.mak

#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#
include .make/release.mk
include .make/make.mk
include .make/python.mk
include .make/oci.mk
include .make/docs.mk
include .release

IMG_DIR := $(PWD)/images/ska-mid-cbf-engineering-console

# Test runner - run to completion job in K8s
# name of the pod running the k8s_tests
TEST_RUNNER = test-runner-$(CI_JOB_ID)-$(RELEASE_NAME)

ITANGO_DOCKER_IMAGE = $(CAR_OCI_REGISTRY_HOST)/ska-tango-images-tango-itango:9.3.5

PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=$(IMG_DIR)

PYTHON_VARS_AFTER_PYTEST = -m "not post_deployment"

# PYTHON_BUILD_TYPE = non_tag_setup

PYTHON_SWITCHES_FOR_FLAKE8 = --ignore=E203,E402,E501,F407,W503

PYTHON_LINT_TARGET = $(IMG_DIR)

MCS_TANGO_HOST = tango-host-databaseds-from-makefile-test.$(KUBE_NAMESPACE).svc.$(DOMAIN):10000

run:  ## Run docker container
	docker run --rm $(strip $(OCI_IMAGE)):$(release)

ADD_HOSTS_FILE := $(PWD)/scripts/hosts.out
ADD_HOSTS = $(shell cat $(ADD_HOSTS_FILE))

# overwrite to use a different JSON
TALONDX_CONFIG_FILE := talondx-config.json
TALONDX_CONFIG_FILE_PATH = $(IMG_DIR)/talondx_config/$(TALONDX_CONFIG_FILE)
TALONDX_LOCAL_DIR = $(PWD)/mnt/talondx-config/
MNT_LOCAL_DIR = $(PWD)/mnt/

# server instance to target for DISH packet capture/RDMA Rx
TALON_UNDER_TEST := talon2_test

BITE_CAPTURE_LOCAL_DIR = $(PWD)"/mnt/bite-data/capture-results"
BITE_STATUS_LOCAL_DIR = $(PWD)"/mnt/bite-data/board-status"

SIGNAL_VERIFICATION_VERSION = 0.0.2
COLLECTOR_DOCKER_IMAGE = $(CAR_OCI_REGISTRY_HOST)/ska-mid-cbf-signal-verification-data-collector:${SIGNAL_VERIFICATION_VERSION}
SIGNAL_VERIFICATION_IMAGE = $(CAR_OCI_REGISTRY_HOST)/ska-mid-cbf-signal-verification:${SIGNAL_VERIFICATION_VERSION}
WB_STATE_COUNT_LOCAL_DIR = $(PWD)/mnt/wb-state-count

BOARDS = "2"

HOSTNAME=$(shell hostname)
ifeq ($(HOSTNAME),rmdskadevdu001.mda.ca)
	BITE_IFACE_NAME="enp179s0f0"
	RDMA_IFACE_NAME="enp179s0f1"
else ifeq ($(HOSTNAME),rmdskadevdu002.mda.ca)
	BITE_IFACE_NAME="ens4f0np0"
	RDMA_IFACE_NAME="ens4f1np1"
endif
BITE_MAC_ADDRESS=$(shell ifconfig $(BITE_IFACE_NAME) | grep -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}')
RDMA_IP=$(shell ifconfig $(RDMA_IFACE_NAME) | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*')
RDMA_MAC_ADDRESS=$(shell ifconfig $(RDMA_IFACE_NAME) | grep -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}')
RDMA_MLX=$(shell show_gids | grep v2 | grep $(RDMA_IP) | awk -F ' ' '{print $$1}')
RDMA_GID_IDX=$(shell show_gids | grep v2 | grep $(RDMA_IP) | awk -F ' ' '{print $$3}')

config-tango-dns: ## Create list of additional MCS DS hosts for docker run command
	@. scripts/config-tango-dns.sh $(KUBE_NAMESPACE) $(DOMAIN) $(ADD_HOSTS_FILE)

config-etc-hosts:
	@. scripts/config-etc-hosts.sh $(KUBE_NAMESPACE) $(DOMAIN)

FINAL_DIR_FILE := $(PWD)/scripts/final_dir.out
FINAL_DIR = $(shell cat $(FINAL_DIR_FILE))

ARTIFACTS_POD = $(shell kubectl -n $(KUBE_NAMESPACE) get pod --no-headers --selector=vol=artifacts-admin -o custom-columns=':metadata.name')

capture-datetime-dir:
	@. scripts/create-datetime-dir.sh $(BITE_CAPTURE_LOCAL_DIR) $(FINAL_DIR_FILE)

status-datetime-dir:
	@. scripts/create-datetime-dir.sh $(BITE_STATUS_LOCAL_DIR) $(FINAL_DIR_FILE)

x-jive: config-tango-dns  ## Run Jive with X11
	@chmod 644 $(HOME)/.Xauthority
	@docker run --rm \
	--network host \
	--env DISPLAY \
	--env TANGO_HOST=$(MCS_TANGO_HOST) \
	--volume /tmp/.X11-unix:/tmp/.X11-unix \
	--volume $(HOME)/.Xauthority:/home/tango/.Xauthority \
	$(ADD_HOSTS) artefact.skatelescope.org/ska-tango-images-tango-jive:7.22.5 &

run-interactive: config-tango-dns  ## Run docker in interactive mode
	docker run --rm -it \
	--network host \
	--volume $(TALONDX_LOCAL_DIR):/app/images/$(strip $(OCI_IMAGE))/artifacts:rw \
	--env TANGO_HOST=$(MCS_TANGO_HOST) \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE)):$(release) bash

copy-artifacts-pod:
	@if [ -d '$(TALONDX_LOCAL_DIR)' ]; then kubectl cp $(TALONDX_LOCAL_DIR)/ ska-mid-cbf/$(ARTIFACTS_POD):/app/mnt/;else echo "ERROR: Cannot find artifacts at mnt/talondx-config"; fi

config-db: config-tango-dns copy-artifacts-pod ## Configure the database
	@echo Configuring Tango DB at $(MCS_TANGO_HOST) with Talon device servers...
	@docker run --rm \
	--network host \
	--env TANGO_HOST=$(MCS_TANGO_HOST) \
	--volume $(TALONDX_LOCAL_DIR):/app/images/$(strip $(OCI_IMAGE))/artifacts:rw \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE)):$(release) ./talondx.py --config-db

db-list: config-tango-dns  ## List the FQDNs of devices in the database
	@echo Getting list of device servers in Tango DB at $(MCS_TANGO_HOST) ...
	@docker run --rm \
	--network host \
	--env TANGO_HOST=$(MCS_TANGO_HOST) \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE)):$(release) ./talondx.py --db-list

generate-talondx-config:
	@echo Getting list of device servers in Tango DB at $(MCS_TANGO_HOST) ...
	@docker run --rm \
	--network host \
	--env TANGO_HOST=$(MCS_TANGO_HOST) \
	--volume $(TALONDX_LOCAL_DIR):/app/images/$(strip $(OCI_IMAGE))/artifacts:rw \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE)):$(release) ./talondx.py --generate-talondx-config --boards=$(BOARDS)

download-artifacts:  ## Download artifacts from CAR and copy the on command sequence script
	mkdir -p $(TALONDX_LOCAL_DIR)
	@docker run --rm -ti \
	--env RAW_USER_ACCOUNT=$(RAW_USER_ACCOUNT) \
	--env RAW_USER_PASS=$(RAW_USER_PASS) \
	--volume $(TALONDX_LOCAL_DIR):/app/images/$(strip $(OCI_IMAGE))/artifacts:rw \
	$(strip $(OCI_IMAGE)):$(release) ./talondx.py --download-artifacts

ds-override-local:
	python3 ds_local_build.py ds_local_build.json $(ds_list) $(ds_basedir) $(mcs_dir)

mcs-itango3:   ## open itango3 shell in MCS
	kubectl exec -it cbfcontroller-controller-0 -n ska-mid-cbf -- itango3

BITE_DATA_SRC=lstv
talon-bite-config: 
	@docker run -ti \
	--network host \
	--env TALON_UNDER_TEST=$(TALON_UNDER_TEST) \
	--env "BITE_DATA_SRC=$(BITE_DATA_SRC)" \
	--env "TANGO_HOST=$(MCS_TANGO_HOST)" \
	--env BITE_MAC_ADDR=$(BITE_MAC_ADDRESS) \
	--volume $(TALONDX_LOCAL_DIR):/app/images/$(strip $(OCI_IMAGE))/artifacts:rw \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE)):$(release) ./talondx.py --talon-bite-config

dish-packet-capture: config-tango-dns capture-datetime-dir
	mkdir -p $(BITE_CAPTURE_LOCAL_DIR)
	@docker run -ti --rm \
	--network host \
	--env "TANGO_HOST=$(MCS_TANGO_HOST)" \
	--env "CAPTURE_DIR=/app/images/$(strip $(OCI_IMAGE))/bite-capture-results" \
	--env BITE_IFACE_NAME=$(BITE_IFACE_NAME) \
	--volume "$(FINAL_DIR):/app/images/$(strip $(OCI_IMAGE))/bite-capture-results:rw" \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE)):$(release) ./talondx.py --dish-packet-capture

write-board-status: config-tango-dns status-datetime-dir
	mkdir -p $(BITE_STATUS_LOCAL_DIR)
	@docker run --rm \
	--network host \
	--env "TANGO_HOST=$(MCS_TANGO_HOST)" \
	--env "TALONDX_STATUS_OUTPUT_DIR=/app/talon-board-status" \
	--volume $(FINAL_DIR):/app/images/$(strip $(OCI_IMAGE))/talon-board-status:rw \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE)):$(release) ./talondx.py --write-board-status


talon-bite-lstv-replay: config-tango-dns
	@docker run --rm \
	--network host \
	--env "TANGO_HOST=$(MCS_TANGO_HOST)" \
	--env "BITE_DATA_SRC=$(BITE_DATA_SRC)" \
	--volume $(TALONDX_LOCAL_DIR):/app/images/$(strip $(OCI_IMAGE))/artifacts:rw \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE)):$(release) ./talondx.py --talon-bite-lstv-replay

talon-version: config-tango-dns
	@docker run --rm \
	--network host \
	--env "TANGO_HOST=$(MCS_TANGO_HOST)" \
	--volume $(TALONDX_LOCAL_DIR):/app/images/$(strip $(OCI_IMAGE))/artifacts:rw \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE)):$(release) ./talondx.py --talon-version

talon-status: config-tango-dns
	@docker run --rm \
	--network host \
	--env "TANGO_HOST=$(MCS_TANGO_HOST)" \
	--env TERM=xterm \
	--volume $(TALONDX_LOCAL_DIR):/app/images/$(strip $(OCI_IMAGE))/artifacts:rw \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE)):$(release) ./talondx.py --talon-status

talon-power-status:
	@docker run --rm \
	--network host \
	--env "POWER_SWITCH_USER=$(POWER_SWITCH_USER)" \
	--env "POWER_SWITCH_PASS=$(POWER_SWITCH_PASS)" \
	$(strip $(OCI_IMAGE)):$(release) ./talondx.py --talon-power-status

mcs-off: config-tango-dns
	@docker run --rm \
	--env "TANGO_HOST=$(MCS_TANGO_HOST)" \
	--network host \
	--env "POWER_SWITCH_USER=$(POWER_SWITCH_USER)" \
	--env "POWER_SWITCH_PASS=$(POWER_SWITCH_PASS)" \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE)):$(release) ./talondx.py --mcs-off

mcs-on: config-tango-dns
	@docker run --rm \
	--network host \
	--env TANGO_HOST=$(MCS_TANGO_HOST) \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE)):$(release) ./talondx.py --mcs-on

mcs-vcc-scan: config-tango-dns
	@docker run --rm \
	--network host \
	--env TANGO_HOST=$(MCS_TANGO_HOST) \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE)):$(release) ./talondx.py --mcs-vcc-scan

talondx-log-consumer: config-tango-dns ## Run the TalonDxLogConsumer Tango device to collect logs from HPS devices
	@docker run --rm \
	--env TANGO_HOST=$(MCS_TANGO_HOST) \
	--network host \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE)):$(release) python ./talondx_log_consumer/talondx_log_consumer.py talondxlogconsumer-001 -ORBendPoint giop:tcp:142.73.34.173:60721 -ORBendPointPublish giop:tcp:169.254.100.88:60721

rdma-rx: config-tango-dns ## Run the RDMA Rx device
	@docker run --rm \
	--network host \
	--env TANGO_HOST=$(MCS_TANGO_HOST) \
	--volume $(PWD)/mnt/talondx-config/dsrdmarx:/app/images/$(strip $(OCI_IMAGE))-rdma/dsrdmarx \
	--device=/dev/infiniband/uverbs0 \
	--device=/dev/infiniband/uverbs1 \
	--device=/dev/infiniband/rdma_cm \
	$(ADD_HOSTS) ska-mid-cbf-engineering-console-rdma:$(release) ./dsrdmarx/bin/dsrdmarx $(TALON_UNDER_TEST) -v4

rdma-on-commands: config-tango-dns ## Trigger the RDMA Rx Commands to connect to the Tx and transfer data
	@docker run --rm \
	--env TALON_UNDER_TEST=$(TALON_UNDER_TEST) \
	--env TANGO_HOST=$(MCS_TANGO_HOST) \
	--env RDMA_MLX=$(RDMA_MLX) \
	--env RDMA_MAC_ADDR=$(RDMA_MAC_ADDRESS) \
	--env RDMA_GID_IDX=$(RDMA_GID_IDX) \
	--env RDMA_IP=$(RDMA_IP) \
	--network host \
	--volume $(TALONDX_LOCAL_DIR):/app/images/$(strip $(OCI_IMAGE))/artifacts:rw \
	$(ADD_HOSTS) $(strip $(OCI_IMAGE)):$(release) ./talondx.py --rdma-on-commands

## runs WB state count capture and saves the vectors at $WB_STATE_COUNT_LOCAL_DIR 
wb-state-count-capture: config-tango-dns 
	@mkdir -p ${WB_STATE_COUNT_LOCAL_DIR}
	@chmod 777 ${WB_STATE_COUNT_LOCAL_DIR}
	@docker run --rm \
	--network=host \
	-v ${WB_STATE_COUNT_LOCAL_DIR}:/app/results:rw \
	--env TANGO_HOST=${MCS_TANGO_HOST} \
	--env WB_STATE_COUNT_DEVICE=talondx-001/wbstatecount/state_count \
	--env OUTPUT_DIR=/app/results ${ADD_HOSTS} \
	${COLLECTOR_DOCKER_IMAGE} python collector.py wb_state_count

## generates a html report using the vectors captured by the data collector.
wb-state-count-report: 
	@docker run --rm \
	--network=host \
	-v ${WB_STATE_COUNT_LOCAL_DIR}:/app/results:rw \
	--env INPUT_DIR=/app/results \
	--env OUTPUT_DIR=/app/results ${ADD_HOSTS} \
	${SIGNAL_VERIFICATION_IMAGE} python run_test.py wb_state_count

documentation:  ## Re-generate documentation
	cd docs && make clean && make html

run-jive:
	docker run --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" -e DISPLAY=$(DISPLAY) -v $(XAUTHORITY):/home/rhuxtable/.Xauthority artefact.skao.int/ska-tango-images-tango-jive:7.22.4

pipeline_unit_test: ## Run simulation mode unit tests in a docker container as in the gitlab pipeline
	@docker run --volume="$$(pwd):/home/tango/ska-tango-examples" \
		--env PYTHONPATH=src:src/ska_tango_examples --env FILE=$(FILE) -it $(ITANGO_DOCKER_IMAGE) \
		sh -c "cd /home/tango/ska-tango-examples && make requirements && make python-test"

help: ## show this help.
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


.PHONY: all test help k8s lint logs describe namespace delete_namespace kubeconfig kubectl_dependencies k8s_test install-chart uninstall-chart reinstall-chart upgrade-chart interactive

