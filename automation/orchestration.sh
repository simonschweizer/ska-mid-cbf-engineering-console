#!/bin/bash

# NOTE: This script must be run as a user with passwordless root permission
# NOTE: This script must be run from the .../automation directory (pwd command)

USAGE_BANNER="Usage: ./orchestration.sh [-n NUM_RUNS] [-q] TALON_A TALON_B
  TALON_A: talon1|talon2|...|talon6 (data is captured from this board)
  TALON_B: talon1|talon2|...|talon6
  
  Options:
  -n NUM_RUNS       Run the test NUM_RUNS times (default=3)
  -q                Skip programming QSPI during setup"

POSITIONAL_ARGS=()
NUM_RUNS=3

unset SKIP_PROGRAM_QSPI

while [[ $# -gt 0 ]]; do
  case $1 in
    -n)   
	    NUM_RUNS="$2"   
      shift # past argument
	    shift # past value
      ;;
    -q)
      echo "Set SKIP_PROGRAM_QSPI"
      export SKIP_PROGRAM_QSPI=1 # used in setup.sh
      shift # past argument
      ;;
    -*|--*)
	  echo "Invalid option $1"
      echo "$USAGE_BANNER"
      exit 1
      ;;
    *)
      POSITIONAL_ARGS+=("$1") # save positional arg
      shift # past argument
      ;;
  esac
done

set -- "${POSITIONAL_ARGS[@]}" # restore positional parameters

if ! [[ "$1" =~ talon1|talon2|talon3|talon4|talon5|talon6 ]]; then
        echo "$USAGE_BANNER"
        exit 1
fi

if ! [[ "$2" =~ talon1|talon2|talon3|talon4|talon5|talon6 ]]; then
        echo "$USAGE_BANNER"
        exit 1
fi

if [ "$1" == "$2" ]; then
        echo "ERROR: TALON_A cannot equal TALON_B. Aborting..."
        exit 1
fi



TALON_A="$1"
TALON_B="$2"

## GLOBAL VARIABLES
TALON_A_NUMBER=$(echo "$TALON_A" | sed 's/[^0-9]*//g')
TALON_B_NUMBER=$(echo "$TALON_B" | sed 's/[^0-9]*//g')

CURR_DIR=$(pwd)
RESULTS_DIR="$CURR_DIR/results"
RESULTS_SUMMARY_FILE="$RESULTS_DIR/results.out"

LOCKFILE=/tmp/automatedTest.running


if [ -e $LOCKFILE ]
then
	echo "Lockfile \"$LOCKFILE\" exists. This test is already running. Aborting..."
        exit 1
fi
touch $LOCKFILE

function cleanup()
{
  minikube delete
	rm $LOCKFILE
}

function setupTestEnvironment()
{
	mkdir -p $RESULTS_DIR
	chmod 777 $RESULTS_DIR
	./setup.sh $TALON_A $TALON_B | tee $RESULTS_DIR/setup.log
	
	if [ "${PIPESTATUS[0]}" -ne "0" ]; then
		echo "Setup failed. Deleting minikube and aborting..." | tee -a $RESULTS_DIR/setup.log
		run_date=$(date +%Y-%m-%d)
		run_time=$(date +%H-%M-%S)
		TEST_RUN_RESULTS_DIR="$RESULTS_DIR/$run_date/$run_time"
		mkdir -p $TEST_RUN_RESULTS_DIR
		cp -p $RESULTS_DIR/setup.log $TEST_RUN_RESULTS_DIR/setup.log

		echo "$run_date,$run_time,Setup Failed,$TEST_RUN_RESULTS_DIR" >> $RESULTS_SUMMARY_FILE

		cleanup
		exit 1
	fi
}

function runTest()
{
	run_date=$(date +%Y-%m-%d)
	run_time=$(date +%H-%M-%S)
	TEST_RUN_RESULTS_DIR="$RESULTS_DIR/$run_date/$run_time"
	FINAL_LOG_DIR="$TEST_RUN_RESULTS_DIR/log"
	mkdir -p $TEST_RUN_RESULTS_DIR
	mkdir -p $FINAL_LOG_DIR
	cp -p $RESULTS_DIR/setup.log $FINAL_LOG_DIR/setup.log
	./script.sh.multiboard "$TALON_A_NUMBER,$TALON_B_NUMBER" ${TALON_A}_test $TEST_RUN_RESULTS_DIR 2>&1 | tee $FINAL_LOG_DIR/test.log
}

# Core
setupTestEnvironment
for ((i = 0; i < $NUM_RUNS; i++));
do
	runTest
done

cleanup
exit 0
