#!/bin/bash

#NOTE: This script must be run as a user with passwordless root permission
#NOTE: This script expects to be run from the .../automation directory
#NOTE: This script must be run as the account that owns the repository (required by git commands)

# Update environment variables to include quartus tools. Needed when running via cronjob.
#source /home/$(whoami)/.bashrc #TODO Cannot update PATH from non-interactive shell. Workaround:
	# License server for Intel Quartus FPGA tools
	LM_LICENSE_FILE=28060@localhost
	
	# Intel Quartus
	FPGA_INTEL=/data/intelFPGA_pro/22.2
	QSYS_ROOTDIR=$FPGA_INTEL/qsys/bin
	PATH=$FPGA_INTEL/quartus/bin:$FPGA_INTEL/quartus/linux64:$PATH
	
	# SKA Project
	FPGA_TOOLS=~/fw_repo/fpga_tools
	REGDEF_PATH=~/fw_repo/registerDef
	PATH=$PATH:/data/share/bin
	
	FPGA_BUILD=$FPGA_TOOLS/fpga_build
	PYTHONPATH=$FPGA_TOOLS:$REGDEF_PATH


USAGE_BANNER="Usage: ./setup.sh TALON_A TALON_B
  TALON_A: talon1|talon2|...|talon6
  TALON_B: talon1|talon2|...|talon6"

if ! [[ "$1" =~ talon1|talon2|talon3|talon4|talon5|talon6 ]]; then
        echo "$USAGE_BANNER"
        exit 1
fi

if ! [[ "$2" =~ talon1|talon2|talon3|talon4|talon5|talon6 ]]; then
        echo "$USAGE_BANNER"
        exit 1
fi

if [ "$1" == "$2" ]; then
        echo "ERROR: TALON_A cannot equal TALON_B. Aborting..."
        exit 1
fi


TALON_A="$1"
TALON_B="$2"

TALON_A_NUMBER=$(echo "$TALON_A" | sed 's/[^0-9]*//g')
TALON_B_NUMBER=$(echo "$TALON_B" | sed 's/[^0-9]*//g')

TALON_A_LRU="$(( ($TALON_A_NUMBER/2) + ($TALON_A_NUMBER%2) ))"
TALON_B_LRU="$(( ($TALON_B_NUMBER/2) + ($TALON_B_NUMBER%2) ))"

## GLOBAL VARIABLES
RED='\033[0;31m'
NC='\033[0m' # No Color

MINIKUBE_PORT="30176"
CURRENT_USER=$(whoami)
ENG_CONSOLE_REPO="/home/$CURRENT_USER/git/ska-mid-cbf-engineering-console"
MINIKUBE_REPO="/home/$CURRENT_USER/git/ska-cicd-deploy-minikube"


function recordConfiguration()
{
	echo ""
	printf "${RED} Recording configuration${NC}\n"
	echo ""
	echo "Setup start time: $(date --iso-8601=seconds)"
	echo ""
	echo "TALON_A: $TALON_A"
	echo "TALON_A_LRU: $TALON_A_LRU"
	echo "TALON_B: $TALON_B"
	echo "TALON_B_LRU: $TALON_B_LRU"
	echo ""
	cd $ENG_CONSOLE_REPO
	echo "Git commit HASH for Automated Script: $(git rev-parse HEAD)"
	git fetch
	git status
}


function startMinikube()
{
	echo ""
	printf "${RED} Starting minikube${NC}\n"
	echo ""
	
	if grep -q "k8s-minikube" <<< $(docker ps | grep minikube); then
		echo "INFO: Minikube already running. Deleting minikube before starting it under current profile."
		minikube delete
		echo ""
		sleep 1
	fi

	echo "INFO: Starting minikube..."
	cd $MINIKUBE_REPO
	make minikube-install DRIVER=docker USE_CACHE=yes
	sleep 3

	echo ""
	echo "INFO: Checking minikube status..."
	MINIKUBE_STATUS=$(minikube status 2>&1)
	echo "$MINIKUBE_STATUS"
	if grep -q "host: Running
	kubelet: Running
	apiserver: Running
	kubeconfig: Configured" <<< "$MINIKUBE_STATUS"; then
		cd $ENG_CONSOLE_REPO
	else
		echo "ERROR: Minikube not running nominally. Aborting..."
		exit 1
	fi
	
	echo ""
	MINIKUBE_IP=$(minikube ip 2>&1)
	echo "Minikube IP: $MINIKUBE_IP"

	EXPECTED_FORWARD_RULE="-A FORWARD -d $MINIKUBE_IP/32 -p tcp -m tcp -j ACCEPT"
	EXPECTED_PREROUTING_RULE="-A PREROUTING -i eno2 -p tcp -m tcp --dport 10001 -j DNAT --to-destination $MINIKUBE_IP:$MINIKUBE_PORT"

	echo "INFO: Updating iptables to match minikube IP"

    sudo iptables-save > ./iptables

	sed -i "/-A FORWARD -d 172.17.0.[1-9]\/32 -p tcp -m tcp -j ACCEPT/c\\$EXPECTED_FORWARD_RULE" ./iptables
	sed -i "/-A PREROUTING -i eno2 -p tcp -m tcp --dport 10001 -j DNAT --to-destination 172.17.0.[1-9]:$MINIKUBE_PORT/c\\$EXPECTED_PREROUTING_RULE" ./iptables

	sudo iptables-restore < ./iptables
	rm ./iptables
	
	echo "INFO: Configuring arp table..."
	arp -H ether -i enp179s0f1 -s 10.50.0.10 02:22:33:44:55:67
}


# Program Talon Boards
function diffScripts()
{
        script=$1
        if [ -n "$(diff /data/share/bin/$script $ENG_CONSOLE_REPO/scripts/$script)" ]; then
                echo "ERROR: $script does not match. Aborting..."
                exit 1
        else
                echo "$script matches."
        fi
}

function pingTalon()
{
        TALON=$1
        ping -c 1 $TALON > /dev/null
        TALON_PING_STATUS=$?
        if [ $TALON_PING_STATUS -eq "0" ]; then
                echo "$TALON: pingable."
                return "0"
        else
                echo "$TALON not pingable."
                return "1"
        fi
}

function restartLRU()
{
        LRU=$1
        echo "Restarting LRU #$LRU..."
        $ENG_CONSOLE_REPO/scripts/talon_power_lru$LRU off
        sleep 5
        $ENG_CONSOLE_REPO/scripts/talon_power_lru$LRU on
        echo "Sleeping 30 seconds..."
        sleep 30
}

function verifyTalonUp()
{
        TALON=$1
        LRU=$2
        pingTalon $TALON
        if [ $? -ne "0" ]; then
                restartLRU $LRU
                pingTalon $TALON
                if [ $? -ne "0" ]; then
                        echo "$TALON still unpingable after LRU restart. Aborting..."
                        exit 1
                fi
        fi
}

function checkTalonConfigurationFile()
{
        echo ""
	echo "INFO: Checking talon configuration file..."
	
	if [ ! -d $ENG_CONSOLE_REPO/automation/mnt ]; then
		echo "$ENG_CONSOLE_REPO/automation/mnt missing. Run automated script manually to initialize. Aborting..."
		exit 1
	fi

        TALON_CONFIG_FILE=$(find $ENG_CONSOLE_REPO/automation/mnt/talondx-config/fpga-talon -name ska-mid-cbf-talondx*)
        echo "INFO: Using Talon DX Config: $TALON_CONFIG_FILE"
        if [ $(echo "$TALON_CONFIG_FILE" | wc -l) -gt "1" ]; then
                echo "ERROR: More than 1 configuration file detected. Aborting..."
                exit 1
        fi
}

function programTalon()
{
        TALON=$1
        echo ""
        echo "INFO: Programming $TALON..."
        cd $ENG_CONSOLE_REPO/scripts
        TALON_PROGRAM_STATUS=$(./program_qspi_$TALON.sh $TALON_CONFIG_FILE 2>&1)
        echo "$TALON_PROGRAM_STATUS"
        if !(grep -q "Quartus Prime Programmer was successful. 0 errors, 0 warnings" <<< "$TALON_PROGRAM_STATUS"); then
                echo "$TALON programming failed. Aborting..."
                exit 1
        fi
}

function programTalonBoards()
{
	echo ""
	printf "${RED} Programming talon boards${NC}\n"
	echo ""
	
	echo "INFO: Verifying /data/share/bin/ matches $ENG_CONSOLE_REPO/scripts/"
	diffScripts "program_qspi_talon$TALON_A_NUMBER.sh"
	diffScripts "program_qspi_talon$TALON_B_NUMBER.sh"
	diffScripts "talon_power_lru$TALON_A_LRU"
	diffScripts "talon_power_lru$TALON_B_LRU"

	echo ""
	echo "INFO: Verifying talon boards are responsive..."
	verifyTalonUp $TALON_A $TALON_A_LRU
	verifyTalonUp $TALON_B $TALON_B_LRU
	
	checkTalonConfigurationFile
	
	echo "SKIP_PROGRAM_QSPI = $SKIP_PROGRAM_QSPI"
	if [ ! -z ${SKIP_PROGRAM_QSPI} ]; then 
		echo "INFO: Skip programming QSPI"
	else
		echo "INFO: Programming QSPI"
		programTalon $TALON_A
		programTalon $TALON_B
	fi
}



# Core
recordConfiguration
startMinikube
programTalonBoards 

echo ""
echo "Setup end time: $(date --iso-8601=seconds)"
echo ""
