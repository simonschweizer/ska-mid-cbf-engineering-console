#!/bin/bash

create_hosts_file () {
	kubectl get services -n "ska-mid-cbf" | while read -r line;
	do
    		svc_split=($line)
    		if [ "${svc_split[1]}" = "LoadBalancer" ]; then
        		host_name="${svc_split[0]%"-external"}.ska-mid-cbf.svc.cluster.local"
        		echo "--add-host=$host_name:${svc_split[3]}" >> hosts.out
    		fi
	done
}

create_directory () {
        dir=$1
        if [[ -z $dir ]]; then echo "No directory provided"; exit 5;fi
        if [[ ! -d $dir ]]; then mkdir -p $dir; fi
}

write_bite_results () {
	run_date=$1
	run_time=$2
	results_base_dir=$3
	results_file=$4
	capture_dir=$5

	x_std=$(cat $capture_dir/statistics.json | jq .x.std)
	y_std=$(cat $capture_dir/statistics.json | jq .y.std)

	if [ $(bc <<< "$x_std >= 0") -eq 1 ] && [ $(bc <<< "$y_std >= 0") -eq 1 ]; then
		summary="$run_date,$run_time,Pass,$results_base_dir"
	else
		summary="$run_date,$run_time,Fail,$results_base_dir"
	fi
	
	echo ""
	echo "$summary"
	echo "$summary" >> $results_file
}

talon_power_lru () {

	OUTLETS="0"
	USER="admin"
	PASS="1234"

	if [[ $2 == "lru_1" ]]; then
		OUTLETS="0"
	elif [[ $2 == "lru_2" ]]; then
		OUTLETS="1"
	elif [[ $2 == "lru_3" ]]; then
		OUTLETS="2"
	elif [[ $2 == "lru_4" ]]; then
		OUTLETS="3"
	fi

	if [[ -n "$1" ]]; then
        	if [[ "$1" =~ on|On|ON ]]; then
                	echo "Set outlets $OUTLETS ON"
                	state="true"
        	elif [[ "$1" =~ off|Off|OFF ]]; then
                	echo "Set outlets $OUTLETS OFF"
                	state="false"
        	else
                	echo "Usage: ./talon_power.sh [on|off]"
                	echo "  Set the power controller outlets to this state."
                	echo "  No argument - return current state."
        	fi;
	fi;

	# Set new state.
	if [[ -n $state ]]; then
        	echo "Changing state ..."
        	curl -s -S -u ${USER}:${PASS} -X PUT -H "X-CSRF: x" --data "value=${state}" \
                	--digest "http://192.168.0.100/restapi/relay/outlets/=${OUTLETS}/state/"
	fi

	# Get current state.
	state=`curl -s -S -u ${USER}:${PASS} -H "Accept: application/json" \
	--digest "http://admin:1234@192.168.0.100/restapi/relay/outlets/=${OUTLETS}/state/"`

	if [[ "$state" == "[false]" ]]; then
	        echo "Talon Power = OFF"
	elif [[ "$state" == "[true]" ]]; then
        	echo "Talon Power = ON"
	else
        	echo "Talon Power = Unknown - raw json = $state"
	fi

}
