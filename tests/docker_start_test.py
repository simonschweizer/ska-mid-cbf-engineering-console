import subprocess

# basic test to check the Engineering Console starts
def test_runContainer():
    result = subprocess.run('make run', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True)
    if(result.stdout.find('Hello from Mid CBF Engineering Console') == -1):
        assert("Engineering Console did not run")



