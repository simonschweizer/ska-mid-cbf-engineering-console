import json
import os
import time

from pytango_client_wrapper import PyTangoClientWrapper
from utils import ip_to_int, mac_to_int


class LmcInterface:
    """Class used to simulate the LMC - Mid.CBF interface."""

    def __init__(self):
        """
        Initializes the device proxies to the relevant MCS devices.
        """
        self.fqdn_cbfcontroller = "mid_csp_cbf/sub_elt/controller"
        self.fqdn_talon_lru = [
            "mid_csp_cbf/talon_lru/001",
        ]
        self.fqdn_powerswitch = "mid_csp_cbf/power_switch/001"
        self.fqdn_vcc_2 = "mid_csp_cbf/vcc/002"

        self.powerswitch = PyTangoClientWrapper()
        self.powerswitch.create_tango_client(self.fqdn_powerswitch)
        self.powerswitch.set_timeout_millis(5000)
        self.powerswitch.write_attribute("simulationMode", 0)
        self.powerswitch.write_attribute("adminMode", 0)

        self.talonlru = []
        for fqdn in self.fqdn_talon_lru:
            dp_talonlru = PyTangoClientWrapper()
            dp_talonlru.create_tango_client(fqdn)
            dp_talonlru.write_attribute("adminMode", 0)
            self.talonlru.append(dp_talonlru)

        self.cbfcontroller = PyTangoClientWrapper()
        self.cbfcontroller.create_tango_client(self.fqdn_cbfcontroller)
        self.cbfcontroller.set_timeout_millis(100000)
        self.cbfcontroller.write_attribute("simulationMode", 0)
        self.cbfcontroller.write_attribute("adminMode", 0)

        self.vcc_2 = PyTangoClientWrapper()
        self.vcc_2.create_tango_client(self.fqdn_vcc_2)
        self.vcc_2.set_timeout_millis(5000)
        self.vcc_2.write_attribute("simulationMode", 0)
        self.vcc_2.write_attribute("adminMode", 0)

    def on_command(self):
        """
        Enables use of the hardware by MCS, and sends the On()
        command to the CbfController.
        """
        self.cbfcontroller.command_read_write("On")

    def off_command(self):
        """
        Enables use of the hardware by MCS, and sends the On()
        command to the CbfController.
        """
        self.cbfcontroller.command_read_write("Off")

    def vcc_scan(self):
        self.vcc_2.command_read_write("On")
        self.vcc_2.command_read_write(
            "ConfigureBand", "1"
        )  # Only bands 1 and 2 are supported by the HPS software
        self.vcc_2.command_read_write(
            "ConfigureScan",
            '{\
            "config_id": "test_config",\
            "frequency_band": "1",\
            "frequency_band_offset_stream_1": 5,\
            "frequency_band_offset_stream_2": 0,\
            "rfi_flagging_mask": "",\
            "fsp": [\
                {\
                   "fsp_id": 1,\
                   "frequency_slice_id": 3,\
                   "function_mode": "CORR"\
                }\
           ]\
        }',
        )  # This is an example of the expected argument format for the VCC
        time.sleep(3)
        self.vcc_2.command_read_write(
            "Scan", "6"
        )  # Use any arbitrary integer ID
        time.sleep(1)
        self.vcc_2.command_read_write("EndScan")

    def rdma_set_attributes(self):
        with open("bite_device_client/json/RDMA_param.json", "r") as rdma_f:
            rdmajson = json.load(rdma_f)
            # Overriding queue pair attributes based on dev server
            rdma_ip = os.getenv("RDMA_IP")
            rdma_mlx = os.getenv("RDMA_MLX")
            rdma_gid_idx = os.getenv("RDMA_GID_IDX")
            rdma_mac_addr = os.getenv("RDMA_MAC_ADDR")

            rdma_ip_int = ip_to_int(rdma_ip)
            rdma_mac_int = mac_to_int(rdma_mac_addr)

            rdma_mac_high = rdma_mac_int >> 32
            rdma_mac_low = rdma_mac_int & 0xFFFF_FFFF

            rdmajson["queuePairAttributes"][0] = rdma_mac_high
            rdmajson["queuePairAttributes"][1] = rdma_mac_low
            rdmajson["queuePairAttributes"][2] = rdma_ip_int
            rdmajson["rdmaRxNetworkDeviceName"] = str(rdma_mlx)
            rdmajson["rdmaGidIndex"] = int(rdma_gid_idx)
        for attribute, value in rdmajson.items():
            print(f"attribute: {attribute}, value: {value}")
            self.rdma_rx.write_attribute(attribute, value)

    def rdma_on_commands(self, rdma_rx_fqdn):
        self.rdma_rx = PyTangoClientWrapper()
        self.rdma_rx.create_tango_client(rdma_rx_fqdn)
        self.rdma_rx.set_timeout_millis(5000)

        self.rdma_set_attributes()
        time.sleep(1)
        self.rdma_rx.command_read_write("connectToRdmaTx")
        time.sleep(1)
        self.rdma_rx.command_read_write("configureAndTransferData")
