#!/usr/bin/env python3
import argparse
import copy
import getpass
import json
import logging
import os
import re
import subprocess
import tarfile
import time
import zipfile
from collections import OrderedDict
from enum import Enum

import pytango_client_wrapper as pcw
import requests
import tango
import tango_db_ops
from bite_device_client.bite_client import BiteClient
from conan_local.conan_wrapper import ConanWrapper
from lmc_interface import LmcInterface
from nrcdbpopulate.dbPopulate import DbPopulate
from requests.structures import CaseInsensitiveDict
from talondx_config.talondx_config import TalonDxConfig
from tango import DeviceProxy
from tqdm import tqdm

LOG_FORMAT = "[talondx.py: line %(lineno)s]%(levelname)s: %(message)s"


class bcolors:
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKCYAN = "\033[96m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    OK = "\x1b[6;30;42m"
    FAIL = "\x1b[0;30;41m"
    ENDC = "\x1b[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


class Target(Enum):
    TALON_1 = "talon1"
    TALON_2 = "talon2"
    TALON_3 = "talon3"
    TALON_4 = "talon4"
    DELL = "dell"


class Version:
    """
    Class to facilitate extracting and comparing version numbers in filenames.

    :param filename: string containing a version substring in the x.y.z format, where x,y,z are numbers.
    """

    def __init__(self, filename):
        [ver_x, ver_y, ver_z] = re.findall("[0-9]+", filename)
        self.X = int(ver_x)
        self.Y = int(ver_y)
        self.Z = int(ver_z)

    def match(self, ver):
        """
        Compare two Version object and return true if the versions match.

        :param ver: Version object being compared to this one.
        """
        return self.X == ver.X and self.Y == ver.Y and self.Z == ver.Z


POWER_SWITCH_USER = os.environ.get("POWER_SWITCH_USER")
POWER_SWITCH_PASS = os.environ.get("POWER_SWITCH_PASS")

PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
ARTIFACTS_DIR = os.path.join(PROJECT_DIR, "artifacts")
TALONDX_CONFIG_FILE = os.path.join(ARTIFACTS_DIR, "talondx-config.json")
DOWNLOAD_CHUNK_BYTES = 1024

TALONDX_STATUS_OUTPUT_DIR = os.environ.get("TALONDX_STATUS_OUTPUT_DIR")

TALON_UNDER_TEST = os.environ.get("TALON_UNDER_TEST")

GITLAB_PROJECTS_URL = "https://gitlab.drao.nrc.ca/api/v4/projects/"
GITLAB_API_HEADER = {
    "PRIVATE-TOKEN": f'{os.environ.get("GIT_ARTIFACTS_TOKEN")}'
}

NEXUS_API_URL = "https://artefact.skatelescope.org/service/rest/v1/"
RAW_REPO_USER = os.environ.get("RAW_USER_ACCOUNT")
RAW_REPO_PASS = os.environ.get("RAW_USER_PASS")


class PowerSwitchState(Enum):
    ON = "ON"
    OFF = "OFF"
    UNKNOWN = "???"


class PowerSwitch:
    """
    Class to manage the network-controlled power switch.
    """

    def __init__(self):
        self._base_url = "http://192.168.0.100/restapi/relay/outlets/"
        # self.state = PowerSwitchState(PowerSwitchState.UNKNOWN)
        self.outlets = "0"  # Only one Talon LRU currently supported

    def state(self):
        """
        Queries the power switch state and returns the result as PowerSwitchState enum
        """
        api_url = f"{self._base_url}={self.outlets}/state/"
        header = CaseInsensitiveDict()
        header["Accept"] = "application/json"
        response = requests.get(
            url=api_url,
            headers=header,
            auth=(POWER_SWITCH_USER, POWER_SWITCH_PASS),
        )
        if response.status_code in [
            requests.codes.ok,  # pylint: disable=no-member
            requests.codes.multi_status,  # pylint: disable=no-member
        ]:
            return self.convert_reponse_to_state(response.text)
        else:
            logger_.info(
                f"Error: unrecognized or failed power switch response: {response}"
            )
            return PowerSwitchState.UNKNOWN

    @staticmethod
    def convert_reponse_to_state(response):
        if "true" in response and "false" not in response:
            return PowerSwitchState.ON
        elif "false" in response and "true" not in response:
            return PowerSwitchState.OFF
        else:
            return PowerSwitchState.UNKNOWN

    def off(self):
        """
        Check the switch state, then powers it off if the switch state is not already off; otherwise no action.
        NOTE: it is strongly recommended to shut down the Talon boards prior to powering off.
        """
        pwr_state = self.state()
        if pwr_state != PowerSwitchState.OFF:
            logger_.info("Powering off...")
            header = CaseInsensitiveDict()
            header["Accept"] = "application/json"
            header["X-CSRF"] = "x"
            header["Content-Type"] = "application/x-www-form-urlencoded"
            data = "value=false"
            response = requests.put(
                url=f"{self._base_url}={self.outlets}/state/",
                data=data,
                headers=header,
                auth=(POWER_SWITCH_USER, POWER_SWITCH_PASS),
            )
            if response.status_code in [
                requests.codes.ok,  # pylint: disable=no-member
                requests.codes.multi_status,  # pylint: disable=no-member
            ]:
                countdown_message(
                    message="Waiting to ensure power off ...", count=5
                )
                # check state after power off
                logger_.info(
                    f"Power Switch (outlets: {self.outlets}): {self.state().value}"
                )
            else:
                logger_.info(
                    f"Power off request failed - response: {response.status_code}, {response.text}"
                )
        else:
            logger_.info(
                f"No action - power switch (outlets: {self.outlets}): {pwr_state.value}"
            )


def countdown_message(message, count, delay_step=1):
    countdown = tqdm(range(count))
    for c in countdown:
        countdown.set_description(f"{message} [{(count - c):>2}]")
        time.sleep(1)
    logger_.info("")


def generate_talondx_config(boards):
    """
    Reads and displays the state and status of each HPS Tango device running on the
    Talon DX boards, as specified in the configuration commands -- ref `"config_commands"`
    in the talondx-config JSON file.

    :param boards: Command delimited list of boards to deploy
    :type boards: str
    """
    boards_list = boards.split(",")
    with open(
        "/app/images/ska-mid-cbf-engineering-console/talondx_config/talondx_boardmap.json",
        "r",
    ) as config_map:
        config_map_json = json.load(config_map)
        talondx_config_dict = {"ds_binaries": config_map_json["ds_binaries"]}
        talondx_config_dict = {
            "fpga_bitstreams": config_map_json["fpga_bitstreams"],
            "ds_binaries": config_map_json["ds_binaries"],
        }
        talondx_config_commands = []
        for board in boards_list:
            talondx_config_commands.append(
                config_map_json["config_commands"][board]
            )
        talondx_config_dict["config_commands"] = talondx_config_commands
        db_servers_list = []
        for db_server in config_map_json["tango_db"]["db_servers"]:
            for board in boards_list:
                db_server_tmp = copy.deepcopy(db_server)
                if db_server["server"] == "dshpsmaster":
                    db_server_tmp["deviceList"][0]["id"] = board
                if db_server["server"] in [
                    "ska-mid-cbf-vcc-app",
                    "ska-mid-cbf-fsp-app",
                    "dshostlutstage1",
                ]:
                    for device in db_server_tmp["deviceList"]:
                        for devprop in device["devprop"]:
                            if "FQDN" in devprop:
                                device["devprop"][devprop] = device["devprop"][
                                    devprop
                                ].replace("<device>", "talondx-00" + board)
                            if "host_lut_stage_2_device_name" in devprop:
                                device["devprop"][devprop] = device["devprop"][
                                    devprop
                                ].replace("<device>", "talondx-00" + board)
                if db_server["server"] == "dsrdmarx":
                    db_server_tmp["deviceList"][0]["devprop"][
                        "rdmaTxTangoDeviceName"
                    ] = db_server_tmp["deviceList"][0]["devprop"][
                        "rdmaTxTangoDeviceName"
                    ].replace(
                        "<device>", "talondx-00" + board
                    )
                    db_server_tmp["deviceList"][0]["alias"] = "rx" + board
                if db_server["server"] != "talondx_log_consumer":
                    db_server_tmp["instance"] = config_map_json[
                        "config_commands"
                    ][board]["server_instance"]
                    if db_server["server"] != "dsrdmarx":
                        db_server_tmp["device"] = "talondx-00" + board
                    db_servers_list.append(db_server_tmp)
            if db_server["server"] == "talondx_log_consumer":
                db_server_tmp = copy.deepcopy(db_server)
                db_servers_list.append(db_server_tmp)
        talondx_config_dict["tango_db"] = {"db_servers": db_servers_list}
        talondx_config_file = open(
            "/app/images/ska-mid-cbf-engineering-console/artifacts/talondx-config.json",
            "w",
        )
        json.dump(talondx_config_dict, talondx_config_file, indent=6)
        talondx_config_file.close()


def configure_db(inputjson):
    """
    Helper function for configuring DB entries using the dbpopulate module.
    """
    # Open bitstream file and load dictionary
    bitstream_f = open(
        "/app/images/ska-mid-cbf-engineering-console/artifacts/fpga-talon/bin/talon_dx-tdc_base-tdc_vcc_processing.json",
        "r",
    )
    bitstream_json = json.load(bitstream_f)
    templates = bitstream_json["DeTrI"]

    for server in inputjson:
        # TODO: make schema validation part of the dbPopulate class
        # with open( "./schema/dbpopulate_schema.json", 'r' ) as sch:
        with open("nrcdbpopulate/schema/dbpopulate_schema.json", "r") as sch:
            # schemajson = json.load(sch, object_pairs_hook=OrderedDict)
            json.load(sch, object_pairs_hook=OrderedDict)
            sch.seek(0)

        # try:
        #     logger_.info( "Validation step")
        #     jsonschema.validate( server, schemajson )
        # except ValidationError as error:
        #     handleValidationError( error, server )
        #     exit(1)

        dbpop = DbPopulate(server, templates)

        # Remove and add to ensure any previous record is overwritten
        dbpop.process(mode="remove")
        dbpop.process(mode="add")


def configure_tango_db(tango_db):
    """
    Configure the Tango DB with devices specified in the talon-config JSON file.

    :param tango_db: JSON string containing the device server specifications for populating the Tango DB
    """
    logger_.info("Configure Tango DB")
    configure_db(inputjson=tango_db.get("db_servers", ""))


def download_git_artifacts(git_api_url, name):
    response = requests.head(url=git_api_url, headers=GITLAB_API_HEADER)

    if response.status_code == requests.codes.ok:  # pylint: disable=no-member
        total_bytes = int(response.headers["Content-Length"])

        response = requests.get(
            git_api_url, headers=GITLAB_API_HEADER, stream=True
        )

        ds_artifacts_dir = os.path.join(ARTIFACTS_DIR, name)
        filename = os.path.join(ds_artifacts_dir, "artifacts.zip")
        bytes_downloaded = 0
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, "wb") as fd:
            for chunk in response.iter_content(
                chunk_size=DOWNLOAD_CHUNK_BYTES
            ):
                fd.write(chunk)
                bytes_downloaded = min(
                    bytes_downloaded + DOWNLOAD_CHUNK_BYTES, total_bytes
                )
                per_cent = round(bytes_downloaded / total_bytes * 100.0)
                logger_.info(
                    f"Downloading {total_bytes} bytes to {os.path.relpath(filename, PROJECT_DIR)} "
                    f"[{bcolors.OK}{per_cent:>3} %{bcolors.ENDC}]",
                    end="\r",
                )
            logger_.info("")

        logger_.info("Extracting files... ", end="")
        with zipfile.ZipFile(filename, "r") as zip_ref:
            zip_ref.extractall(ds_artifacts_dir)
        logger_.info(f"{bcolors.OK}done{bcolors.ENDC}")
    else:
        logger_.info(
            f"{bcolors.FAIL}status: {response.status_code}{bcolors.ENDC}"
        )


def download_raw_artifacts(api_url, name, filename):
    # TODO: factorize common code with download_git_artifacts
    response = requests.head(url=api_url, auth=(RAW_REPO_USER, RAW_REPO_PASS))

    if response.status_code == requests.codes.ok:  # pylint: disable=no-member
        total_bytes = int(response.headers["Content-Length"])

        response = requests.get(
            api_url, auth=(RAW_REPO_USER, RAW_REPO_PASS), stream=True
        )

        artifacts_dir = os.path.join(ARTIFACTS_DIR, name)
        filename = os.path.join(artifacts_dir, filename)
        bytes_downloaded = 0
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, "wb") as fd:
            logger_.info(
                f"Downloading {total_bytes} bytes to {os.path.relpath(filename, PROJECT_DIR)}"
            )
            for chunk in response.iter_content(
                chunk_size=DOWNLOAD_CHUNK_BYTES
            ):
                fd.write(chunk)
                bytes_downloaded = min(
                    bytes_downloaded + DOWNLOAD_CHUNK_BYTES, total_bytes
                )
                per_cent = round(bytes_downloaded / total_bytes * 100.0)
                print(
                    f"Downloading {total_bytes} bytes to {os.path.relpath(filename, PROJECT_DIR)} "
                    f"[{bcolors.OK}{per_cent:>3} %{bcolors.ENDC}]",
                    end="\r",
                )
            print("\n")

        logger_.info("Extracting files... ")
        if tarfile.is_tarfile(filename):
            tar = tarfile.open(filename, "r")
            tar.extractall(artifacts_dir)
            tar.close()
        else:
            with zipfile.ZipFile(filename, "r") as zip_ref:
                zip_ref.extractall(artifacts_dir)
        logger_.info(f"{bcolors.OK}Done extracting files{bcolors.ENDC}")

        # TODO: workaround; request permissions change from systems team
        os.chmod(os.path.join(artifacts_dir, "MANIFEST.skao.int"), 0o644)
    else:
        logger_.info(
            f"{bcolors.FAIL}File extraction failed - status: {response.status_code}{bcolors.ENDC}"
        )


def download_ds_binaries(ds_binaries, clear_conan_cache=True):
    """
    Downloads and extracts Tango device server (DS) binaries from Conan packages
    or Git pipeline artifacts.

    :param ds_binaries: JSON string specifying which DS binaries to download.
    :param clear_conan_cache: if true, Conan packages are fetched from remote; default true.
    """
    conan = ConanWrapper(ARTIFACTS_DIR)
    logger_.info(f"Conan version: {conan.version()}")
    if clear_conan_cache:
        logger_.info(f"Conan local cache: {conan.search_local_cache()}")
        logger_.info(
            f"Clearing Conan local cache... {conan.clear_local_cache()}"
        )
    logger_.info(f"Conan local cache: {conan.search_local_cache()}")

    for ds in ds_binaries:
        logger_.info(f"DS Binary: {ds['name']}")

        if ds.get("source") == "conan":
            # Download the specified Conan package
            conan_info = ds.get("conan")
            logger_.info(f"Conan info: {conan_info}")
            conan.download_package(
                pkg_name=conan_info["package_name"],
                version=conan_info["version"],
                user=conan_info["user"],
                channel=conan_info["channel"],
                profile=os.path.join(
                    conan.profiles_dir, conan_info["profile"]
                ),
            )
        elif ds.get("source") == "git":
            # Download the artifacts from the latest successful pipeline
            git_info = ds.get("git")
            url = (
                f'{GITLAB_PROJECTS_URL}{git_info["git_project_id"]}/jobs/artifacts/'
                f'{git_info["git_branch"]}/download?job={git_info["git_pipeline_job"]}'
            )
            download_git_artifacts(git_api_url=url, name=ds["name"])
        else:
            logger_.info(f'Error: unrecognized source ({ds.get("source")})')
            exit(-1)

    # Modify the permissions of Artifacts dir so they can be modified/deleted later
    chmod_r_cmd = "chmod -R o=rwx " + ARTIFACTS_DIR
    os.system(chmod_r_cmd)


def download_fpga_bitstreams(fpga_bitstreams):
    """
    Downloads and extracts FPGA bitstreams from the CAR (Common Artefact Repository),
    or Git pipeline artifacts.

    :param fpga_bitstreams: JSON string specifying which FPGA bitstreams to download.
    """
    for fpga in fpga_bitstreams:
        req_version = Version(fpga["version"])
        if fpga.get("source") == "raw":
            # Download the bitstream from the raw repo in CAR
            raw_info = fpga.get("raw")
            logger_.info(
                f"FPGA bitstream {raw_info['base_filename']}-{fpga['version']}"
            )
            # url = f"{NEXUS_API_URL}search?repository=raw-internal&group=/{raw_info['group']}/*"
            # TODO: update to filter directly on "base_filename"
            url = f"{NEXUS_API_URL}search?repository=raw-internal&group=/"
            response = requests.get(
                url=url, auth=(RAW_REPO_USER, RAW_REPO_PASS)
            )
            download_urls = []
            if (
                response.status_code
                == requests.codes.ok  # pylint: disable=no-member
            ):
                for item in response.json().get("items", []):
                    # logger_.info(f'\nRaw search response item: {item}')
                    for asset in item.get("assets"):
                        download_urls.append(asset.get("downloadUrl"))
            else:
                logger_.info(response)

            for file_url in download_urls:
                logger_.info(f"file_url = {file_url}")
                filename_pattern = (
                    f"{raw_info['base_filename']}-{fpga['version']}.tar.gz"
                )
                logger_.info(f"filename_pattern = {filename_pattern}")
                filenames = re.findall(filename_pattern, file_url)
                logger_.info(f"filenames: {filenames}")
                if filenames.__len__() == 1:
                    logger_.info(filenames[0])
                    version = Version(filenames[0])
                    if version.match(req_version):
                        logger_.info("Versions match - downloading...")
                        download_raw_artifacts(
                            api_url=file_url,
                            name="fpga-talon",
                            filename=filenames[0],
                        )
        else:
            # Download the artifacts from the latest successful Git pipeline
            git_info = fpga["git"]
            url = (
                f'{GITLAB_PROJECTS_URL}{git_info["git_project_id"]}/jobs/artifacts/'
                f'{git_info["git_branch"]}/download?job={git_info["git_pipeline_job"]}'
            )
            logger_.info(f"GitLab API call for bitstream download: {url}")

            # Alternate URL for downloading specific pipeline job
            """
            url = f"{GITLAB_PROJECTS_URL}{git_info['git_project_id']}/jobs/{git_info['git_pipeline_job']}/artifacts"
            logger_.info(f"GitLab API call for bitstream download: {url}")
            # https://gitlab.drao.nrc.ca/SKA/Mid.CBF/FW/persona/tdc_vcc_processing/-/jobs/12180
            """
            download_git_artifacts(
                git_api_url=url, name=f"fpga-{fpga['target']}"
            )


def secure_copy(target, src, dest):
    target_dest = f"root@{target.value}:{dest}"
    logger_.info(f"Copy: {src:<70} TO {target_dest}")

    proc = subprocess.Popen(["scp", src, target_dest])
    count = 0
    while proc.poll() is None:
        count += 1
        logger_.info("." * count, end="\r")
        time.sleep(0.2)


def make_dir(target, dir):
    logger_.info(f"Creating directory {dir} on {target.value} ...")
    try:
        proc = subprocess.Popen(
            ["ssh", f"root@{target.value}", f"mkdir -p {dir}"]
        )
        count = 0
        while proc.poll() is None:
            count += 1
            logger_.info("." * count, end="\r")
            time.sleep(0.2)
    except Exception as e:
        logger_.info(f"Directory creation failed (Exception: {e}).")


def send_config_command(config, cmd_target=Target.TALON_1):

    config_cmd = [
        cmd
        for cmd in config.config_commands()
        if cmd.get("target") == cmd_target.value
    ][0]
    config_cmd_str = json.dumps(config_cmd)
    logger_.info(config_cmd_str)

    logger_.info(
        "TANGO_HOST = {}".format(tango.ApiUtil.get_env_var("TANGO_HOST"))
    )

    # TODO: refactor common code (ref: start_talon_dx_master())
    for db_server in config.tango_db()["db_servers"]:
        if db_server["server"] == "dshpsmaster":
            inst = db_server["instance"]
            device = db_server["device"]
            name = db_server["name"]

            if (cmd_target == Target.TALON_1 and device == "talondx-001") or (
                cmd_target == Target.TALON_2 and device == "talondx-002"
            ):
                for dev in db_server["deviceList"]:
                    for inst in dev["inst"]:
                        dev_name = (
                            f"{device}/{name}/{dev['alias']}-{inst['id']}"
                        )

                        logger_.info(
                            f"Sending configure command to {dev_name}"
                        )
                        tc = pcw.PyTangoClientWrapper()
                        tc.create_tango_client(dev_name)
                        poll_period = 0
                        tango_db_ops.reset_polling(tc.dp, poll_period)
                        cmd_ret = tc.command_read_write(
                            "configure", config_cmd_str
                        )
                        logger_.info(
                            f"Config command return from {dev_name}: {cmd_ret}"
                        )


def get_device_version_info(config_commands):
    """
    Reads and displays the `dsVersionId`, `dsBuildDateTime`, and `dsGitCommitHash` attributes
    of each HPS Tango device running on the Talon DX boards, as specified in the configuration
    commands -- ref `"config_commands"` in the talondx-config JSON file.

    :param config_commands: JSON array of configure commands
    :type config_commands: str
    """
    targets = [Target.TALON_1, Target.TALON_2, Target.TALON_3]
    for target in targets:
        logger_.info("================")
        logger_.info(f"Target: {target.value}")
        logger_.info("================")
        config_cmd = [
            cmd for cmd in config_commands if cmd.get("target") == target.value
        ][0]

        devices = config_cmd["devices"]
        devices.insert(0, "dshpsmaster")

        for dev_name in get_device_fqdns(
            devices, config_cmd["server_instance"]
        ):
            try:
                dev_proxy = DeviceProxy(dev_name)

                if dev_proxy.import_info().exported:
                    logger_.info(f"{dev_proxy.info().dev_class:<20}{dev_name}")
                    attr_names = [
                        "dsVersionId",
                        "dsBuildDateTime",
                        "dsGitCommitHash",
                    ]
                    for attr_name in attr_names:
                        try:
                            attr_value = dev_proxy.read_attribute(attr_name)
                            logger_.info(
                                f"  {attr_value.name:<20}: {attr_value.value}"
                            )
                        except Exception as attr_except:
                            logger_.info(
                                f"Error reading attribute: {attr_except}"
                            )
                else:
                    logger_.info(f"{dev_name}   DEVICE NOT EXPORTED!")
            except Exception as proxy_except:
                logger_.info(
                    f"Error on DeviceProxy ({dev_name}): {proxy_except}"
                )


def get_device_fqdn_list():
    """
    Get full list of fully-qualified device names (FQDNs) from the Tango database, excluding "sys" and "dserver" names.

    Ref: https://tango-controls.readthedocs.io/en/latest/tutorials-and-howtos/how-tos/how-to-pytango.html

    :returns: alphabetically-sorted list of FQDNs (str)
    """
    try:
        db = tango.Database()
    except Exception as db_except:
        logger_.info(f"Database error: {db_except}")
        exit()

    instances = []
    for server in db.get_server_list():
        # Filter out the unwanted items from the list to get just the FQDNs of our devices...
        # the full list from get_device_class_list() has the structure:
        # [device name, class name, device name, class name, ...]
        # and also includes the admin server (dserver/exec_name/instance)
        instances += [
            dev
            for dev in db.get_device_class_list(server)
            if "/" in dev
            and not dev.startswith("dserver")
            and not dev.startswith("sys")
        ]

    return sorted(instances)


def get_device_fqdns(devices, server_inst):
    """
    Generate list of fully-qualified device names (FQDNs) from Tango database for the
    given list of devices and server instance.

    Ref: https://tango-controls.readthedocs.io/en/latest/tutorials-and-howtos/how-tos/how-to-pytango.html

    :param devices: device names
    :type devices: list of str
    :param server_inst: server instance
    :type server_inst: string

    :returns: list of FQDNs (str)
    """
    try:
        db = tango.Database()
    except Exception as db_except:
        logger_.info(f"Database error: {db_except}")
        exit()

    dev_names = []
    server_list = db.get_server_list()
    for ds in devices:
        for server in server_list:
            if server_inst in server and ds in server:
                # Filter out the unwanted items from the list to get just the FQDNs of our devices...
                # the full list from get_device_class_list() has the structure:
                # [device name, class name, device name, class name, ...]
                # and also includes the admin server (dserver/exec_name/instance)
                dev_names += [
                    dev
                    for dev in db.get_device_class_list(server)
                    if "/" in dev and not dev.startswith("dserver")
                ]

    return dev_names


def get_device_status(config_commands):
    """
    Reads and displays the state and status of each HPS Tango device running on the
    Talon DX boards, as specified in the configuration commands -- ref `"config_commands"`
    in the talondx-config JSON file.

    :param config_commands: JSON array of configure commands
    :type config_commands: str
    """
    targets = [Target.TALON_1, Target.TALON_2]
    for target in targets:
        logger_.info("================")
        logger_.info(f"Target: {target.value}")
        logger_.info("================")
        config_cmd = [
            cmd for cmd in config_commands if cmd.get("target") == target.value
        ][0]

        devices = copy.deepcopy(config_cmd["devices"])
        devices.insert(0, "dshpsmaster")

        for dev_name in get_device_fqdns(
            devices, config_cmd["server_instance"]
        ):
            try:
                dev_proxy = DeviceProxy(dev_name)
            except Exception as proxy_except:
                logger_.info(
                    f"Error on DeviceProxy {dev_name}: {proxy_except}"
                )
                break
            if dev_proxy.import_info().exported:
                try:
                    ds_state = str(dev_proxy.state())
                    ds_status = dev_proxy.status()
                    logger_.info(
                        f"{dev_name:<50}: state {ds_state:<8}  status={ds_status}"
                    )
                except Exception as status_except:
                    logger_.info(
                        f"Error reading state or status of {dev_name}: {status_except}"
                    )
            else:
                logger_.info(f"{dev_name}   DEVICE NOT EXPORTED!")


if __name__ == "__main__":
    logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
    logger_ = logging.getLogger("talondx.py")
    logger_.info(f"User: {getpass.getuser()}")
    parser = argparse.ArgumentParser(description="Talon DX Utility")
    parser.add_argument(
        "-v",
        "--verbose",
        help="increase output verbosity",
        action="store_true",
    )
    parser.add_argument(
        "--config-db",
        help="configure the Tango database with devices specified in talondx-config.json file",
        action="store_true",
    )
    parser.add_argument(
        "--db-list",
        help="list the FQDNs (fully qualified domain names) of the HPS and MCS devices in the Tango database",
        action="store_true",
    )
    parser.add_argument(
        "--download-artifacts",
        help="download the FPGA bitstreams and Tango DS binaries from the Common Artefact Repository (CAR)",
        action="store_true",
    )
    parser.add_argument(
        "--talon-bite-config",
        help="configure the BITE devices on the Talon DX board",
        action="store_true",
    )
    parser.add_argument(
        "--talon-bite-lstv-replay",
        help="Start LSTV Replay on the Talon DX board",
        action="store_true",
    )
    parser.add_argument(
        "--dish-packet-capture",
        help="Start the Dish Packet Capture",
        action="store_true",
    )
    parser.add_argument(
        "--talon-version",
        help="get the version information of the Tango devices running on the Talon DX boards",
        action="store_true",
    )
    parser.add_argument(
        "--talon-status",
        help="get the status information of the Tango devices running on the Talon DX boards",
        action="store_true",
    )
    parser.add_argument(
        "--talon-power-status",
        help="get the status of the Talon LRU power supply",
        action="store_true",
    )
    parser.add_argument(
        "--mcs-off",
        help="run the MCS Off command sequence",
        action="store_true",
    )
    parser.add_argument(
        "--mcs-on", help="run the MCS On command sequence", action="store_true"
    )
    parser.add_argument(
        "--rdma-on-commands",
        help="set the rdma on commands to connect to tx and transfer data",
        action="store_true",
    )
    parser.add_argument(
        "--mcs-vcc-scan", help="run the scan vcc commands", action="store_true"
    )
    parser.add_argument(
        "--write-talon-status",
        help="write talon board status to file",
        action="store_true",
    )
    parser.add_argument(
        "--generate-talondx-config",
        help="Generate talondx config file",
        action="store_true",
    )
    parser.add_argument(
        "--boards",
        type=str,
    )
    args = parser.parse_args()

    if args.config_db:
        logger_.info(
            f'Configure DB - TANGO_HOST = {tango.ApiUtil.get_env_var("TANGO_HOST")}'
        )
        config = TalonDxConfig(config_file=TALONDX_CONFIG_FILE)
        configure_tango_db(config.tango_db())
    elif args.db_list:
        logger_.info("DB List")
        for inst in get_device_fqdn_list():
            logger_.info(inst)
    elif args.download_artifacts:
        logger_.info("Download Artifacts")
        config = TalonDxConfig(config_file=TALONDX_CONFIG_FILE)
        config.export_config(ARTIFACTS_DIR)
        download_ds_binaries(config.ds_binaries())
        download_fpga_bitstreams(config.fpga_bitstreams())
    elif args.talon_bite_config:
        logger_.info("Talon BITE Configure")
        config = TalonDxConfig(config_file=TALONDX_CONFIG_FILE)
        for command in config.config_commands():
            bite = BiteClient(command["server_instance"], False)
            bite.init_devices(
                "bite_device_client/json/device_server_list.json"
            )
            bite.configure_bite(talon_inst=TALON_UNDER_TEST)
    elif args.talon_bite_lstv_replay:
        logger_.info("Talon BITE LSTV Replay")
        config = TalonDxConfig(config_file=TALONDX_CONFIG_FILE)
        for command in config.config_commands():
            bite = BiteClient(command["server_instance"], False)
            bite.init_devices(
                "bite_device_client/json/device_server_list.json"
            )
            bite.start_lstv_replay()
    elif args.dish_packet_capture:
        logger_.info("Dish Packet Capture")
        subprocess.run(
            "python3 ./mellanox_dish_packet_capture/src/PlotSampleData.py ./mellanox_dish_packet_capture/src/default_inputs.json ./mellanox_dish_packet_capture/src/default_inputs.json",
            shell=True,
        )
    elif args.talon_version:
        logger_.info("Talon Version Information")
        config = TalonDxConfig(config_file=TALONDX_CONFIG_FILE)
        get_device_version_info(config.config_commands())
    elif args.talon_status:
        logger_.info("Talon Status Information")
        config = TalonDxConfig(config_file=TALONDX_CONFIG_FILE)
        while True:
            os.system("clear")
            get_device_status(config.config_commands())
            time.sleep(2)
    elif args.talon_power_status:
        pwr = PowerSwitch()
        logger_.info(
            f"Power Switch (outlets: {pwr.outlets}): {pwr.state().value}"
        )
    elif args.mcs_off:
        lmc_interface = LmcInterface()
        lmc_interface.off_command()
    elif args.mcs_on:
        lmc_interface = LmcInterface()
        lmc_interface.on_command()
    elif args.mcs_vcc_scan:
        lmc_interface = LmcInterface()
        lmc_interface.vcc_scan()
    elif args.rdma_on_commands:
        config = TalonDxConfig(config_file=TALONDX_CONFIG_FILE)
        for command in config.config_commands():
            if command["server_instance"] == TALON_UNDER_TEST:
                lmc_interface = LmcInterface()
                lmc_interface.rdma_on_commands(
                    rdma_rx_fqdn=command["ds_rdma_rx_fqdn"]
                )
    elif args.write_talon_status:
        logger_.info("Print Talon Status")
        config = TalonDxConfig(config_file=TALONDX_CONFIG_FILE)
        for command in config.config_commands():
            if "ska-talondx-status-ds" in command["devices"]:
                bite = BiteClient(command["server_instance"], False)
                bite.init_devices(
                    "bite_device_client/json/device_server_list.json"
                )
                bite.write_talon_status(
                    "bite_device_client/json/status_attr_list.json",
                    TALONDX_STATUS_OUTPUT_DIR,
                )
    elif args.generate_talondx_config and args.boards is not None:
        logger_.info("Generate talondx-config.json file")
        generate_talondx_config(args.boards)
    else:
        logger_.info("Hello from Mid CBF Engineering Console!")
