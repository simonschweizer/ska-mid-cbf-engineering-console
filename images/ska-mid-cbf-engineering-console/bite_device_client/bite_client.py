from __future__ import annotations

import json
import logging
import math
import os
import time
from datetime import datetime, timezone
from typing import Any

import numpy as np
import tango
import tango_db_ops
import tqdm
from jsonschema import validate
from tango import DeviceProxy

THIS_DIR = os.path.dirname(os.path.abspath(__file__))
DEVICE_SERVER_LIST_SCHEMA = os.path.join(
    THIS_DIR, "device_server_list_schema.json"
)
LOG_FORMAT = (
    "[BiteClient.%(funcName)s: line %(lineno)s]%(levelname)s: %(message)s"
)

# Constants
TARGET_SAMPLE_RATE = 220200960  # samples per second (at the FSP, post retimer, must be this for the 16k Channeliser)
VCC_FRAME_SIZE = 18  # Number of samples input to the VCC.
VCC_CHANNELS = 20  # Number of oversampled channels output by the VCC. Oversampling factor = VCC_CHANNELS/VCC_FRAME_SIZE.
LSTV_SAMPLES_PER_DDR_WORD = 21
LSTV_DDR_WORD_IN_BYTES = 64
DDR_SIZE = 8  # GiB
LTA_BANKS = 2

# BITE shares memory with the long term accumulator; LTA at low addresses, BITE above.
PACKET_RATE_SCALE_FACTOR = (
    0.01  # Can't run at full speed because sharing DDR with LTA.
)
# 1.0 = approximately real time as from DISH. 0.01 = Rate that the DELL server running UDP can keep up with.

CORRELATOR_NUM_COLUMNS = 8
CORRELATOR_ANT_PER_COLUMN = 20
CORRELATOR_NUM_CHANNELS = 744

NUM_TAPS = 1024


class BiteClient:
    """
    Class to configure the low level BITE devices
    """

    # Utility functions.

    def _ceil_pow2(self: BiteClient, val: int) -> int:
        return 2 ** math.ceil(math.log(val, 2))

    def __init__(self: BiteClient, inst: str, log_to_UML: bool) -> None:
        """
        Initialize BITE parameters

        :param inst: name of Talon HPS DS server instance
        :type inst: str
        :param log_to_UML: set to True to print BITE output to UML format
        :type log_to_UML: bool
        """
        logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
        self._log_to_UML = log_to_UML
        self._logger = logging.getLogger("BiteClient")
        self._server_inst = inst
        self._dp_dict = {}
        self._device_servers = None
        self._receiver_selector = int(0)
        self._source_selector = int(0)
        self._tone_selector = int(0)
        self._filter_energy_rcv_X = 0
        self._filter_energy_rcv_Y = 0
        self._rx_gen = {}
        self._src_gen = []
        self._rfi = []
        self._fir_filter_gen_out_rcv_file_name = ""
        self._num_antenna = (
            CORRELATOR_NUM_COLUMNS * CORRELATOR_ANT_PER_COLUMN
        )  # pseudo antenna in the case of TDC
        self._lta_baselines = self._num_antenna * (self._num_antenna - 1) // 2
        self._lta_end_addr = (
            self._ceil_pow2(LTA_BANKS)
            * self._ceil_pow2(CORRELATOR_NUM_CHANNELS)
            * self._ceil_pow2(self._lta_baselines)
        )  # 64 byte words
        self._logger.info(
            f"LTA end address = LSTV start address: {self._lta_end_addr*LSTV_DDR_WORD_IN_BYTES/2**30} GiB"
        )
        self._lstv_start_word_addr = self._lta_end_addr
        self._lstv_end_word_addr = (
            DDR_SIZE * 2**30 // LSTV_DDR_WORD_IN_BYTES - 1
        )
        assert self._lstv_end_word_addr > self._lstv_start_word_addr
        self._logger.info(
            f"LSTV end address: {self._lstv_end_word_addr*LSTV_DDR_WORD_IN_BYTES/2**30} GiB"
        )
        self._logger.info(
            f"LSTV size = {(self._lstv_end_word_addr-self._lstv_start_word_addr)*LSTV_SAMPLES_PER_DDR_WORD/1e9:1.3f} billion samples."
        )
        self._sample_rate = TARGET_SAMPLE_RATE * VCC_FRAME_SIZE
        src_index = dict(sstv=0, ramp=1, lstv=2)
        self._src_select = src_index[os.getenv("BITE_DATA_SRC")]
        self._logger.info(f"Source selected: {self._src_select}")

    def init_devices(self: BiteClient, input_file_name: str) -> None:
        """
        Initialize the device servers from the JSON file

        :param input_file_name: the name of the JSON file containing
            the device server data
        """
        self._input_file_name = input_file_name
        self._device_servers = json.load(open(input_file_name))
        with open(DEVICE_SERVER_LIST_SCHEMA, "r") as schema_fd:
            bite_client_schema = json.load(schema_fd)
        validate(instance=self._device_servers, schema=bite_client_schema)
        for name in self._device_servers:
            self._create_dps_for_server(name)

    def configure_bite(self: BiteClient, talon_inst: str) -> None:
        """
        Configure BITE devices
        """
        self._logger.info("Entering ...")
        if not self._device_servers:
            self._logger.error("init_devices not called")
            return

        try:
            # stop any previous LSTV replay
            self._write_attribute(self._dp_dict["lstv_pbk"], "run", 0)

            # stop/reset any previous LSTV generation
            self._command_read_write(
                self._dp_dict["lstv_gen"], "ip_control", False
            )
            if self._dp_dict["lstv_gen"].read_attribute("ip_status").value:
                raise Exception("LSTV Generation already in progress.")

            # configure noise sources
            self._src_gen = self._device_servers["dsgaussiannoisegen"][
                "source"
            ]

            # TODO: Update the scaling factor in the future when the factor of two is removed
            self._fir_filter_gen_out_rcv_file_name = self._device_servers[
                "dsgaussiannoisegen"
            ]["fir_filter_gen_out_rcv_file_name"]
            conf = json.load(open(self._fir_filter_gen_out_rcv_file_name))

            self._filter_energy_rcv_X = conf["filter_params"][0]["h_fxp_energ"]
            self._filter_energy_rcv_Y = conf["filter_params"][1]["h_fxp_energ"]

            source_mean_polX = np.zeros(4, dtype=int)
            source_mean_polY = np.zeros(4, dtype=int)
            source_std_polX = np.zeros(4, dtype=int)
            source_std_polY = np.zeros(4, dtype=int)

            for n in range(0, min(len(self._src_gen), 4)):
                if self._src_gen[n]["select"] == 1:
                    for pol in ("X", "Y"):
                        # mean fixed to 0, std fixed to max
                        # The actual standard deviation of the noise source is:
                        #   stddev = sqrt( 1 / (3 * g_ORDER)) = 0.144, where g_ORDER = 16.
                        self._write_attribute(
                            self._dp_dict[f"gn_gen_src_pol{pol}_{n}"],
                            "noise_mean",
                            self._src_gen[n][f"noise_info_pol{pol}"][
                                "noise_mean"
                            ],
                        )
                        self._write_attribute(
                            self._dp_dict[f"gn_gen_src_pol{pol}_{n}"],
                            "noise_std",
                            self._src_gen[n][f"noise_info_pol{pol}"][
                                "noise_std"
                            ],
                        )
                        self._write_attribute(
                            self._dp_dict[f"gn_gen_src_pol{pol}_{n}"],
                            "seed_ln",
                            self._src_gen[n][f"noise_info_pol{pol}"]["seed"]
                            + ord(pol),
                        )
                        self._write_attribute(
                            self._dp_dict[f"gn_gen_src_pol{pol}_{n}"],
                            "seed_cos",
                            self._src_gen[n][f"noise_info_pol{pol}"]["seed"]
                            + ord(pol)
                            + 1,
                        )

                    source_mean_polX[n] = (
                        self._src_gen[n]["noise_info_polX"]["noise_mean"]
                        * 2**4
                    )
                    source_mean_polY[n] = (
                        self._src_gen[n]["noise_info_polY"]["noise_mean"]
                        * 2**4
                    )

                    stdScale = 1
                    if stdScale > 1:
                        self._logger.warning("std exceeds maximum")
                    else:
                        source_std_polX[n] = int(stdScale * (2**16 - 1))

                    if stdScale > 1:
                        self._logger.warning("std exceeds maximum")
                    else:
                        source_std_polY[n] = int(stdScale * (2**16 - 1))

                # configure polarisation coupler
                self._pol_coupler = self._device_servers["dsgaussiannoisegen"][
                    "pol_coupler"
                ]
                rho = self._pol_coupler[n]["rho"]
                self._write_attribute(
                    self._dp_dict[f"pol_coupler_{n}"],
                    "delay_enable",
                    self._pol_coupler[n]["delay_enable"],
                )
                self._write_attribute(
                    self._dp_dict[f"pol_coupler_{n}"],
                    "alpha",
                    int(rho * (2**17 - 1)),
                )
                self._write_attribute(
                    self._dp_dict[f"pol_coupler_{n}"],
                    "beta",
                    int(math.sqrt(1 - rho**2) * (2**16 - 1)),
                )

            self._rx_gen = self._device_servers["dsgaussiannoisegen"][
                "receiver"
            ]

            for pol in ("X", "Y"):
                self._write_attribute(
                    self._dp_dict[f"gn_gen_rcv_pol{pol}"],
                    "noise_mean",
                    self._rx_gen[f"noise_info_pol{pol}"]["noise_mean"],
                )
                self._write_attribute(
                    self._dp_dict[f"gn_gen_rcv_pol{pol}"],
                    "noise_std",
                    self._rx_gen[f"noise_info_pol{pol}"]["noise_std"],
                )
                self._write_attribute(
                    self._dp_dict[f"gn_gen_rcv_pol{pol}"],
                    "seed_ln",
                    self._rx_gen[f"noise_info_pol{pol}"]["seed"] + ord(pol),
                )
                self._write_attribute(
                    self._dp_dict[f"gn_gen_rcv_pol{pol}"],
                    "seed_cos",
                    self._rx_gen[f"noise_info_pol{pol}"]["seed"]
                    + ord(pol)
                    + 1,
                )

            h_fxp_int16_polX = conf["filter_params"][0]["h_fxp_int16"]
            h_fxp_int16_polY = conf["filter_params"][1]["h_fxp_int16"]

            self._filter_coeffs_rcv_polX = np.squeeze(
                np.array(h_fxp_int16_polX, dtype=np.int32)
            )
            self._filter_coeffs_rcv_polY = np.squeeze(
                np.array(h_fxp_int16_polY, dtype=np.int32)
            )

            if (
                self._filter_coeffs_rcv_polX.shape[0] > 0
                and self._filter_coeffs_rcv_polY.shape[0] > 0
            ):
                self._write_attribute(
                    self._dp_dict["fir_filt_rcv_polX"],
                    "filter_coeff",
                    self._filter_coeffs_rcv_polX,
                )
                self._write_attribute(
                    self._dp_dict["fir_filt_rcv_polY"],
                    "filter_coeff",
                    self._filter_coeffs_rcv_polY,
                )
                self._write_attribute(
                    self._dp_dict["fir_filt_src_polX_0"],
                    "filter_coeff",
                    self._filter_coeffs_rcv_polX,
                )
                self._write_attribute(
                    self._dp_dict["fir_filt_src_polY_0"],
                    "filter_coeff",
                    self._filter_coeffs_rcv_polY,
                )

            else:
                self._logger.error("Filter coefficient numpy shape incorrect.")

            self._rfi = self._device_servers["dsgaussiannoisegen"]["rfi"]
            for n, tone in enumerate(self._rfi):
                self._tone_selector |= int(tone["select"]) << n
            self._command_read_write(
                self._dp_dict["lstv_gen"], "tone_select", self._tone_selector
            )

            for n in range(0, min(len(self._rfi), 4)):
                if self._rfi[n]["select"] == 1:
                    for pol in ("X", "Y"):
                        self._write_attribute(
                            self._dp_dict[f"tone_gen_pol{pol}"],
                            "mag_scale",
                            self._rfi[n][f"tone_info_pol{pol}"]["magnitude"],
                        )
                        self._write_attribute(
                            self._dp_dict[f"tone_gen_pol{pol}"],
                            "phase_inc",
                            self._rfi[n][f"tone_info_pol{pol}"]["phase_inc"],
                        )

                    break  # assuming only a single RFI tone

            # configure LSTV generator
            self._command_read_write(
                self._dp_dict["lstv_gen"], "ip_control", False
            )
            if self._dp_dict["lstv_gen"].read_attribute("ip_status").value:
                raise Exception("LSTV Generation already in progress.")

            # allocate memory for LSTV, start address, in units of 64 bytes
            self._write_attribute(
                self._dp_dict["lstv_gen"],
                "ddr4_start_addr",
                self._lstv_start_word_addr,
            )
            self._write_attribute(
                self._dp_dict["lstv_gen"],
                "ddr4_end_addr",
                self._lstv_end_word_addr,
            )

            # select sources
            for n, source in enumerate(self._src_gen):
                self._source_selector |= int(source["select"]) << n
            self._command_read_write(
                self._dp_dict["lstv_gen"],
                "source_select",
                self._source_selector,
            )
            self._receiver_selector = self._select_input(
                self._rx_gen["select"], 0, self._receiver_selector
            )
            self._command_read_write(
                self._dp_dict["lstv_gen"],
                "receiver_select",
                self._receiver_selector,
            )

            self._write_attribute(
                self._dp_dict["lstv_gen"], "source_mean_polX", source_mean_polX
            )
            self._write_attribute(
                self._dp_dict["lstv_gen"], "source_mean_polY", source_mean_polY
            )
            # TODO: Do not hardcode
            source_std_polX[0] = 2**16 - 1
            source_std_polY[0] = 2**16 - 1
            self._write_attribute(
                self._dp_dict["lstv_gen"], "source_std_polX", source_std_polX
            )
            self._write_attribute(
                self._dp_dict["lstv_gen"], "source_std_polY", source_std_polY
            )
            # TODO: when lstvGen incldes polarization coupler info, do the same for it

            # TODO: Update the scaling factor in the future when the factor of two is removed
            self._write_attribute(
                self._dp_dict["lstv_gen"],
                "receiver_mean_polX",
                self._rx_gen["noise_info_polX"]["noise_mean"] * 2**4,
            )
            self._write_attribute(
                self._dp_dict["lstv_gen"],
                "receiver_mean_polY",
                self._rx_gen["noise_info_polY"]["noise_mean"] * 2**4,
            )
            stdScale = 1
            if stdScale > 1:
                self._logger.warning("std exceeds maximum")
            else:
                self._write_attribute(
                    self._dp_dict["lstv_gen"],
                    "receiver_std_polX",
                    stdScale * (2**16 - 1),
                )

            if stdScale > 1:
                self._logger.warning("std exceeds maximum")
            else:
                self._write_attribute(
                    self._dp_dict["lstv_gen"],
                    "receiver_std_polY",
                    stdScale * (2**16 - 1),
                )

            self._command_read_write(
                self._dp_dict["lstv_gen"], "ip_control", True
            )
            start = time.time()
            seconds_per_GB = 3.5  # FIXME so the estimate is about accurate.
            est_time = (
                (self._lstv_end_word_addr - self._lstv_start_word_addr)
                * 64
                / 2**30
                * seconds_per_GB
            )
            self._logger.info(
                f"Long sequence test vector is generating, will take approximately {est_time:1.1f} seconds ..."
            )
            with tqdm.tqdm(total=math.ceil(est_time * 2) / 2) as pbar:
                while (
                    self._dp_dict["lstv_gen"].read_attribute("ip_status").value
                ):
                    time.sleep(0.5)
                    pbar.update(0.5)
            # Stop LSTV generation
            end = time.time()
            self._command_read_write(
                self._dp_dict["lstv_gen"], "ip_control", False
            )
            self._logger.info(
                f"Long sequence test vector finished generation, took {end-start:1.1f} seconds."
            )

            # configure SPFRx Packetizer
            self._command_read_write(
                self._dp_dict["spfrx_pkt"], "bringup", 123
            )
            self._write_attribute(
                self._dp_dict["spfrx_pkt"],
                "sample_rate_band12",
                self._sample_rate,
            )
            # configure local/remote mac if chosen talon for packet capture
            bite_mac_addr = os.getenv("BITE_MAC_ADDR").replace(":", "")
            bite_mac_addr_hex = int(bite_mac_addr, 16)
            if talon_inst == self._server_inst:
                self._write_attribute(
                    self._dp_dict["spfrx_pkt"], "rem_mac", bite_mac_addr_hex
                )
                self._write_attribute(
                    self._dp_dict["spfrx_pkt"], "loc_mac", 0x102233445566
                )

            # set 100G ethernet port 0 in loopback
            self._command_read_write(self._dp_dict["100g_eth_0"], "bringup", 1)

        except Exception as e:
            self._logger.error(f"{str(e)}")

    def start_lstv_replay(self: BiteClient) -> None:
        """
        Start LSTV Replay
        """
        self._logger.info("Entering ...")
        if not self._device_servers:
            self._logger.error("init_devices not called")
            return

        try:
            lstv_seconds = (
                (self._lstv_end_word_addr - self._lstv_start_word_addr)
                * LSTV_SAMPLES_PER_DDR_WORD
                / self._sample_rate
            )
            self._logger.info(
                f"LSTV repeats after {lstv_seconds:1.3f} seconds."
            )

            # hold the dish packet generator in reset
            self._write_attribute(self._dp_dict["lstv_pbk"], "run", 0)
            self._write_attribute(
                self._dp_dict["lstv_pbk"], "sample_rate", self._sample_rate - 1
            )
            ref_clk_freq = (
                self._dp_dict["lstv_pbk"].read_attribute("ref_clk_freq").value
            )

            # word_rate is a 32b value such that when it is accumulated a change in msb triggers a new word.
            samples_per_cycle = (
                self._sample_rate / ref_clk_freq * PACKET_RATE_SCALE_FACTOR
            )
            self._logger.info(f"samples_per_cycle = {samples_per_cycle}")
            self._write_attribute(
                self._dp_dict["lstv_pbk"],
                "samples_per_cycle",
                int(samples_per_cycle * 2**32),
            )

            self._write_attribute(
                self._dp_dict["lstv_pbk"],
                "start_utc_time_code",
                int(
                    datetime.utcnow().replace(tzinfo=timezone.utc).timestamp()
                ),
            )

            self._write_attribute(
                self._dp_dict["lstv_pbk"],
                "lstv_start_addr",
                self._lstv_start_word_addr,
            )
            self._write_attribute(
                self._dp_dict["lstv_pbk"],
                "lstv_end_addr",
                self._lstv_end_word_addr,
            )

            self._write_attribute(self._dp_dict["lstv_pbk"], "run", 1)
        except Exception as e:
            self._logger.error(f"{str(e)}")

    def write_talon_status(
        self: BiteClient,
        status_attr_filename: str,
        talondx_status_output_dir: str,
    ) -> None:
        """
        Print Talon Status
        """
        self._logger.info("Entering ...")
        if not self._device_servers:
            self._logger.error("init_devices not called")
            return
        try:
            self._create_dps_for_server("ska-talondx-status-ds")
            status_attr_file = open(status_attr_filename)
            status_attr_json = json.load(status_attr_file)
            status_dict = {}
            for attr in status_attr_json:
                attr_status = (
                    self._dp_dict["status"].read_attribute(attr).value
                )
                if type(attr_status) == np.ndarray:
                    attr_status = attr_status.tolist()
                status_dict[attr] = attr_status
            out_status_filename = os.path.join(
                talondx_status_output_dir,
                self._server_inst + "_talon_status.json",
            )
            out_status_file = open(out_status_filename, "w")
            json.dump(status_dict, out_status_file, indent=4)
            out_status_file.close()
        except Exception as e:
            self._logger.error(f"{str(e)}")

    def _select_input(
        self: BiteClient, jsonSelector: int, n: int, selector: int
    ) -> int:
        if jsonSelector == 1:
            return selector | 0x1 << int(n)
        return selector

    def _command_read_write(
        self: BiteClient,
        dp: tango.DeviceProxy,
        command_name: str,
        input_args=None,
    ) -> None:
        """
        Wrapper function that calls a tango command

        :param dp: the device proxy whose command will be called
        :param command_name: the command name
        :param input_args: the command input arguments (None if the command has no arguments)
        """
        try:
            if self._log_to_UML:
                print(
                    f"BiteClient -> {dp.dev_name()} ** : CMD {command_name}({input_args})"
                )
            else:
                self._logger.info(
                    f"command_read_write({dp.dev_name()}, {command_name}, {input_args})"
                )
            return dp.command_inout(command_name, input_args)
        except Exception as e:
            self._logger.error(str(e))

    def _write_attribute(
        self: BiteClient, dp: tango.DeviceProxy, attr_name: str, attr_val: Any
    ) -> None:
        """
        Wrapper function that writes to a tango device attribute

        :param dp: the device proxy whose attribute we will be writen
        :param attr_name: the name of the attribute to be written
        :param attr_val: the value to be written to the attribute
        """
        try:
            if self._log_to_UML:
                print(
                    f"BiteClient -> {dp.dev_name()} ** : {attr_name} = {attr_val})"
                )
            else:
                self._logger.info(
                    f"write_attribute({dp.dev_name()}, {attr_name}, {attr_val})"
                )
            return dp.write_attribute(attr_name, attr_val)
        except Exception as e:
            self._logger.error(str(e))

    def _create_dps_for_server(self: BiteClient, serverName: str) -> None:
        """
        Get a list of device names from the specified device server and create proxies
            for each device name

        :param serverName: the name of the device server
        """

        server_full_name = serverName + "/" + self._server_inst

        db_dev_list = tango_db_ops.get_existing_devs(server_full_name)

        if db_dev_list is not None:

            for db_dev in db_dev_list:

                if self._create_deviceProxy(db_dev):
                    self._logger.info(
                        "Created device proxy for {}".format(db_dev)
                    )
                else:
                    self._logger.error(
                        "Error on creating device proxy for  {}".format(db_dev)
                    )
        else:
            self._logger.error(
                "The server {} is not running".format(serverName)
            )

    def _create_deviceProxy(self: BiteClient, deviceName: str) -> bool:
        """
        Create a device proxy using the device name

        :param deviceName: the name of the device
        """
        try:
            key = deviceName.split("/")[-1]
            self._logger.info(
                "Creating device server for {}".format(deviceName)
            )
            self._dp_dict[key] = DeviceProxy(deviceName)
        except tango.DevFailed as df:
            for item in df.args:
                self._logger.error(
                    f"Failed to create proxy for {deviceName} : {item.reason} {item.desc}"
                )
            return False
        return True
