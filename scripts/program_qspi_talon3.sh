#!/bin/bash

ssh root@talon3 -n shutdown now
sleep 5

archive=$1
qspi_file=`tar --wildcards --get -vf $archive *.jic`
usb_jtag="USB-BlasterII [1-10.4.3]"
jtagconfig --setparam "$usb_jtag" JtagClock 6M
quartus_pgm -c "$usb_jtag" -m jtag -o "pvi;./$qspi_file"
rm $qspi_file
