
**************************
BITE Client 
**************************

.. automodule:: bite_device_client.bite_client.BiteClient
    :members:

************************************
BITE Configuration and LSTV playback
************************************

.. uml:: diagrams/bite-sequence.puml
